<!doctype html>
<?php
$server     = 'localhost'; //servidor
$username   = 'root'; //usuario de la base de datos
$password   = ''; //password del usuario de la base de datos
$database   = 'muebles'; //nombre de la base de datos
 
$conexion = @new mysqli($server, $username, $password, $database);
 
if ($conexion->connect_error) 
{
    die('Error de conexión: ' . $conexion->connect_error); 
}
 
$sql="SELECT * from articulos";
$result = $conexion->query($sql); 
 
if ($result->num_rows > 0) 
{
    $combobit="";
	$combonum="";
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) 
    {
        $combobit .=" <option value='".$row['Descripcion']."'>".$row['Descripcion']."</option>";	
    }
}
else
{
    echo "No hubo resultados";
}
$conexion->close(); 
?>

<html>
<head>
	<meta charset="utf-8">
<title>Venta</title>
<style type="text/css">
			
			* {
				margin:0px;
				padding:0px;
			}
			
			#header {
				margin:auto;
				width:left;
				font-family:Arial, Helvetica, sans-serif;
			}
			
			ul, ol {
				list-style:none;
			}
			
			.nav > li {
				float:left;
			}
			
			.nav li a {
				background-color:#000;
				color:#fff;
				text-decoration:none;
				padding:10px 12px;
				display:block;
			}
			
			.nav li a:hover {
				background-color:#434343;
			}
			
			.nav li ul {
				display:none;
				position:absolute;
				min-width:140px;
			}
			
			.nav li:hover > ul {
				display:block;
			}
			
			.nav li ul li {
				position:relative;
			}
			
			.nav li ul li ul {
				right:-140px;
				top:0px;
			}
			
		</style>
	
	
</head>
	
<style>
 
input:required:invalid {
 
border: 1px solid red;
 
}
	</style>
<body>
   
   
   <header><ul class="nav">	
				<li><a href="Main.html">INICIO</a>
					<ul>
						<li><a href="VentasGeneral.php">Venta</a></li>
						<li><a href="">Clientes</a></li>
						<li><a href="Articulos.php">Articulos</a></li>		
					</ul>
				</li>
  </ul>
	   <?php 
$time = time();
echo date("d-m-Y", $time); ?>
	</header>
<form action="ResumenVenta.php" method="post">
	
<table width="409" height="158" border="0" >
  <tbody>
	    <tr>
	      <th width="108">Ingrese su nombre</th>
	      <td width="142"><input type="text" name="Nombre" id="Nombre" required></td>
	      <td width="145">&nbsp;</td>
        </tr>
	    <tr>
	      <th height="62" scope="row">Favor de Seleccionar un Producto</th>
	      <td><ul class="nav">
	        <select name="Descrip" id="Descrip">
	          <?php echo $combobit;?>
			  </select>
          </ul></td>
	      <td>Unidades a Comprar
			  <input type="number" name="Exist" id="Exist" required min=0>
			
		  </td>
        </tr>
	    <tr>
	      <th scope="row">
			</th>
	      <td>&nbsp;</td>
	      <td>
			  
			  <input type="submit" value="Continuar" >
			  
			</td>
        </tr>
      </tbody>
</table>
	</form>
	
</body>
</html>