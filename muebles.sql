-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 14-06-2018 a las 17:07:40
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `muebles`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `Clave` int(5) NOT NULL,
  `Descripcion` varchar(100) NOT NULL,
  `Modelo` varchar(40) NOT NULL,
  `Precio` int(10) NOT NULL,
  `Existencia` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `articulos`
--

INSERT INTO `articulos` (`Clave`, `Descripcion`, `Modelo`, `Precio`, `Existencia`) VALUES
(1, 'Silla', 'Silla Sencilla', 600, 37),
(3, 'Sillon', 'Reclinable', 2500, 17),
(4, 'Mecedora', 'Ãšnico', 2400, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas`
--

CREATE TABLE `ventas` (
  `FolioVenta` int(5) NOT NULL,
  `ClaveCliente` int(5) NOT NULL,
  `NombreCliente` varchar(40) NOT NULL,
  `Total` decimal(10,0) NOT NULL,
  `Fecha` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ventas`
--

INSERT INTO `ventas` (`FolioVenta`, `ClaveCliente`, `NombreCliente`, `Total`, `Fecha`) VALUES
(1, 1, 'Ramos', '6500', '12/6/18'),
(29, 1, 'Fernando', '4766', '14-06-2018'),
(30, 1, 'Fernando', '5505', '14-06-2018'),
(31, 1, 'REyes', '4766', '14-06-2018'),
(32, 1, 'Ramon', '0', '14-06-2018'),
(33, 1, '', '0', '14-06-2018'),
(34, 1, 'Aguilar', '4404', '14-06-2018'),
(35, 1, 'Fernando', '477', '14-06-2018'),
(36, 1, 'Fernando', '1541', '14-06-2018');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`Clave`);

--
-- Indices de la tabla `ventas`
--
ALTER TABLE `ventas`
  ADD PRIMARY KEY (`FolioVenta`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `Clave` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `ventas`
--
ALTER TABLE `ventas`
  MODIFY `FolioVenta` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
